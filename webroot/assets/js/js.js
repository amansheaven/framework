$(document).ready(function() {
    $('#select-table-dropdown').on('change', function() {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        var baseUrl = window.location.origin;
        
        var newUrl = baseUrl + '/stats/' + valueSelected;
        
        window.location.href = newUrl;
    });
});