# This starts the docker services.
service docker start

# Enable the docker instances.
systemctl enable docker

# Run cdlidev.
./dev/cdlidev.py up -d

# Currently the size of database is is huge hence, we need a curl url to 
# install the database (TODO: @NishealJ)

# - docker cp ./app/private/database/cdli_db.sql cdlidev_mariadb_1:/tmp/cdli_db.sql
# - docker exec -it cdlidev_mariadb_1  mysql -e "create database cdli_db;"
  