<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading text-center">Enter 2FA Code</div>
        <?= $this->Flash->render() ?>

        <?= $this->Form->create() ?>
            <?= $this->Form->control('code', ['class' => 'form-control', 'autocomplete' => 'off']) ?>
            <?= $this->Form->submit(); ?>

            <?php if(env('APP_ENV') === 'development'): ?>
                <div class="text-center my-2">
                    <div class="col-md-12"><a href="/" class="btn btn-primary" role="button">Skip</a></div>
                </div>
            <?php endif; ?>

        <?= $this->Form->end() ?>
    </div>

</div>
