<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AccountingPeriod $accountingPeriod
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <!-- <li><?= $this->Html->link(__('Edit Accounting Period'), ['action' => 'edit', $accountingPeriod->id]) ?> </li> -->
        <!-- <li><?= $this->Form->postLink(__('Delete Accounting Period'), ['action' => 'delete', $accountingPeriod->id], ['confirm' => __('Are you sure you want to delete # {0}?', $accountingPeriod->id)]) ?> </li> -->
        <li><?= $this->Html->link(__('List Accounting Periods'), ['action' => 'index']) ?> </li>
        <!-- <li><?= $this->Html->link(__('New Accounting Period'), ['action' => 'add']) ?> </li> -->
        <li><?= $this->Html->link(__('List Rulers'), ['controller' => 'Rulers', 'action' => 'index']) ?> </li>
        <!-- <li><?= $this->Html->link(__('New Ruler'), ['controller' => 'Rulers', 'action' => 'add']) ?> </li> -->
        <li><?= $this->Html->link(__('List Months'), ['controller' => 'Months', 'action' => 'index']) ?> </li>
        <!-- <li><?= $this->Html->link(__('New Month'), ['controller' => 'Months', 'action' => 'add']) ?> </li> -->
    </ul>
</nav>
<div class="accountingPeriods view large-9 medium-8 columns content">
    <h3><?= h($accountingPeriod->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($accountingPeriod->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ruler') ?></th>
            <td><?= $accountingPeriod->has('ruler') ? $this->Html->link($accountingPeriod->ruler->id, ['controller' => 'Rulers', 'action' => 'view', $accountingPeriod->ruler->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Month') ?></th>
            <td><?= $accountingPeriod->has('month') ? $this->Html->link($accountingPeriod->month->id, ['controller' => 'Months', 'action' => 'view', $accountingPeriod->month->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($accountingPeriod->end_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($accountingPeriod->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Ruler Id') ?></th>
            <td><?= $this->Number->format($accountingPeriod->start_ruler_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Month Id') ?></th>
            <td><?= $this->Number->format($accountingPeriod->start_month_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Year Id') ?></th>
            <td><?= $this->Number->format($accountingPeriod->start_year_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Year Id') ?></th>
            <td><?= $this->Number->format($accountingPeriod->end_year_id) ?></td>
        </tr>
    </table>
</div>
