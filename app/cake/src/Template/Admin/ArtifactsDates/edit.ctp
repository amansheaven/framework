<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsDate $artifactsDate
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsDate) ?>
            <legend class="capital-heading"><?= __('Edit Artifacts Date') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts, 'empty' => true]);
                echo $this->Form->control('date_id', ['options' => $dates, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $artifactsDate->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsDate->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts Dates'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Dates'), ['controller' => 'Dates', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Date'), ['controller' => 'Dates', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
