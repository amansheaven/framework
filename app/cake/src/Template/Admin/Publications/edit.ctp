<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($publication) ?>
            <legend class="capital-heading"><?= __('Edit Publication') ?></legend>
            <?php
                echo $this->Form->control('bibtexkey');
                echo $this->Form->control('year');
                echo $this->Form->control('entry_type_id', ['options' => $entryTypes, 'empty' => true]);
                echo $this->Form->control('address');
                echo $this->Form->control('annote');
                echo $this->Form->control('book_title');
                echo $this->Form->control('chapter');
                echo $this->Form->control('crossref');
                echo $this->Form->control('edition');
                echo $this->Form->control('editor');
                echo $this->Form->control('how_published');
                echo $this->Form->control('institution');
                echo $this->Form->control('journal_id', ['options' => $journals, 'empty' => true]);
                echo $this->Form->control('month');
                echo $this->Form->control('note');
                echo $this->Form->control('number');
                echo $this->Form->control('organization');
                echo $this->Form->control('pages');
                echo $this->Form->control('publisher');
                echo $this->Form->control('school');
                echo $this->Form->control('title');
                echo $this->Form->control('volume');
                echo $this->Form->control('publication_history');
                echo $this->Form->control('abbreviation_id', ['options' => $abbreviations, 'empty' => true]);
                echo $this->Form->control('series');
                echo $this->Form->control('oclc');
                echo $this->Form->control('designation');
                // echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
                echo $this->Form->control('authors._ids', ['options' => $authors]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $publication->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $publication->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Entry Types'), ['controller' => 'EntryTypes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Entry Type'), ['controller' => 'EntryTypes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Journals'), ['controller' => 'Journals', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Journal'), ['controller' => 'Journals', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Abbreviations'), ['controller' => 'Abbreviations', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Abbreviation'), ['controller' => 'Abbreviations', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
