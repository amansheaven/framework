<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Genre $genre
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= h($genre->genre) ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Genre') ?></th>
                    <td><?= h($genre->genre) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Parent Genre') ?></th>
                    <td><?= $genre->has('parent_genre') ? $this->Html->link($genre->parent_genre->genre, ['controller' => 'Genres', 'action' => 'view', $genre->parent_genre->id]) : '' ?></td>
                </tr>
                <!-- <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($genre->id) ?></td>
                </tr> -->
                <tr>
                    <th scope="row"><?= __('Genre Comments') ?></th>
                    <td><?= $this->Text->autoParagraph(h($genre->genre_comments)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Genre'), ['action' => 'edit', $genre->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Genre'), ['action' => 'delete', $genre->id], ['confirm' => __('Are you sure you want to delete # {0}?', $genre->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Genres'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Genre'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Parent Genres'), ['controller' => 'Genres', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Parent Genre'), ['controller' => 'Genres', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Child Genres'), ['controller' => 'Genres', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Child Genre'), ['controller' => 'Genres', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>


<div class="boxed mx-0">
    <?php if ($count==0): ?>
        <div class="capital-heading"><?= __('No Related Artifacts') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Related Artifacts') ?></div>
        <p><a href="/search">Number of related artifacts - <?=$count?></a></p>
    <?php endif; ?>
</div>

<div class="boxed mx-0">
    <?php if (empty($genre->child_genres)): ?>
        <div class="capital-heading"><?= __('No Child Genres') ?></div>
    <?php else: ?>
        <div class="capital-heading"><?= __('Child Genres') ?></div>
        <table cellpadding="0" cellspacing="0" class="table-bootstrap">
            <thead>
<!--                 <th scope="col"><?= __('Id') ?></th>
 -->                <th scope="col"><?= __('Genre') ?></th>
                <th scope="col"><?= __('Genre Comments') ?></th>
<!--                 <th scope="col"><?= __('Actions') ?></th>
 -->            </thead>
            <tbody>
                <?php foreach ($genre->child_genres as $childGenres): ?>
                <tr>
<!--                     <td><?= h($childGenres->id) ?></td>
 -->                    <td><a href="/genres/<?= h($childGenres->id) ?>"><?= h($childGenres->genre) ?></a></td>
                    <td><?= h($childGenres->genre_comments) ?></td>
                    <!-- <td class="d-flex flex-row">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                            ['controller' => 'Genres', 'action' => 'view', $childGenres->id],
                            ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['controller' => 'Genres', 'action' => 'edit', $childGenres->id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                        <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['controller' => 'Genres', 'action' => 'delete', $childGenres->id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $childGenres->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                    </td> -->
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif; ?>
</div>


