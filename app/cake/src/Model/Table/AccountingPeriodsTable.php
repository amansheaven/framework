<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AccountingPeriods Model
 *
 * @property \App\Model\Table\RulersTable|\Cake\ORM\Association\BelongsTo $Rulers
 * @property \App\Model\Table\MonthsTable|\Cake\ORM\Association\BelongsTo $Months
 * @property \App\Model\Table\YearsTable|\Cake\ORM\Association\BelongsTo $Years
 * @property \App\Model\Table\RulersTable|\Cake\ORM\Association\BelongsTo $Rulers
 * @property \App\Model\Table\MonthsTable|\Cake\ORM\Association\BelongsTo $Months
 * @property \App\Model\Table\YearsTable|\Cake\ORM\Association\BelongsTo $Years
 *
 * @method \App\Model\Entity\AccountingPeriod get($primaryKey, $options = [])
 * @method \App\Model\Entity\AccountingPeriod newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AccountingPeriod[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AccountingPeriod|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccountingPeriod|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccountingPeriod patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AccountingPeriod[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AccountingPeriod findOrCreate($search, callable $callback = null, $options = [])
 */
class AccountingPeriodsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounting_periods');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Rulers', [
            'foreignKey' => 'start_ruler_id'
        ]);
        $this->belongsTo('Months', [
            'foreignKey' => 'start_month_id'
        ]);
        $this->belongsTo('Years', [
            'foreignKey' => 'start_year_id'
        ]);
        $this->belongsTo('Rulers', [
            'foreignKey' => 'end_ruler_id'
        ]);
        $this->belongsTo('Months', [
            'foreignKey' => 'end_month_id'
        ]);
        $this->belongsTo('Years', [
            'foreignKey' => 'end_year_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('start_date')
            ->maxLength('start_date', 10)
            ->allowEmpty('start_date');

        $validator
            ->scalar('end_date')
            ->maxLength('end_date', 10)
            ->allowEmpty('end_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['start_ruler_id'], 'Rulers'));
        $rules->add($rules->existsIn(['start_month_id'], 'Months'));
        $rules->add($rules->existsIn(['start_year_id'], 'Years'));
        $rules->add($rules->existsIn(['end_ruler_id'], 'Rulers'));
        $rules->add($rules->existsIn(['end_month_id'], 'Months'));
        $rules->add($rules->existsIn(['end_year_id'], 'Years'));

        return $rules;
    }
}
