<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string|null $username
 * @property \Cake\I18n\FrozenTime|null $created
 * @property string|null $created_by
 * @property bool|null $admin
 * @property int|null $download_hd_images
 * @property string|null $filtering
 * @property bool|null $can_download_hd_images
 * @property bool|null $can_view_private_catalogues
 * @property bool|null $can_view_private_transliterations
 * @property bool|null $can_edit_transliterations
 * @property bool|null $can_view_private_images
 * @property bool|null $can_view_iPadWeb
 * @property string|null $email
 * @property int|null $author_id
 * @property string|null $password
 * @property string|null $2fa_key
 * @property bool|null $2fa_status
 *
 * @property \App\Model\Entity\Author $author
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'created' => true,
        'created_by' => true,
        'admin' => true,
        'download_hd_images' => true,
        'filtering' => true,
        'can_download_hd_images' => true,
        'can_view_private_catalogues' => true,
        'can_view_private_transliterations' => true,
        'can_edit_transliterations' => true,
        'can_view_private_images' => true,
        'can_view_iPadWeb' => true,
        'email' => true,
        'author_id' => true,
        'password' => true,
        '2fa_key' => true,
        '2fa_status' => true,
        'author' => true
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];

    //hash password
    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }
}
