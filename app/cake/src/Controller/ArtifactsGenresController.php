<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsGenres Controller
 *
 * @property \App\Model\Table\ArtifactsGenresTable $ArtifactsGenres
 *
 * @method \App\Model\Entity\ArtifactsGenre[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsGenresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Genres']
        ];
        $artifactsGenres = $this->paginate($this->ArtifactsGenres);

        $this->set(compact('artifactsGenres'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Genre id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsGenre = $this->ArtifactsGenres->get($id, [
            'contain' => ['Artifacts', 'Genres']
        ]);

        $this->set('artifactsGenre', $artifactsGenre);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsGenre = $this->ArtifactsGenres->newEntity();
        if ($this->request->is('post')) {
            $artifactsGenre = $this->ArtifactsGenres->patchEntity($artifactsGenre, $this->request->getData());
            if ($this->ArtifactsGenres->save($artifactsGenre)) {
                $this->Flash->success(__('The artifacts genre has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts genre could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsGenres->Artifacts->find('list', ['limit' => 200]);
        $genres = $this->ArtifactsGenres->Genres->find('list', ['limit' => 200]);
        $this->set(compact('artifactsGenre', 'artifacts', 'genres'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Genre id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsGenre = $this->ArtifactsGenres->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsGenre = $this->ArtifactsGenres->patchEntity($artifactsGenre, $this->request->getData());
            if ($this->ArtifactsGenres->save($artifactsGenre)) {
                $this->Flash->success(__('The artifacts genre has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts genre could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsGenres->Artifacts->find('list', ['limit' => 200]);
        $genres = $this->ArtifactsGenres->Genres->find('list', ['limit' => 200]);
        $this->set(compact('artifactsGenre', 'artifacts', 'genres'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Genre id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsGenre = $this->ArtifactsGenres->get($id);
        if ($this->ArtifactsGenres->delete($artifactsGenre)) {
            $this->Flash->success(__('The artifacts genre has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts genre could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
